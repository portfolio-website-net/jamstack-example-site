var apiUrl = 'https://aws.random.cat/meow'

$(function() {                                
    displayPhoto()
})

function displayPhoto() {
    $.ajax(
        {
            url: apiUrl, 
            type: 'GET',
            success: function(result) {
                $('#pnlLoading').hide()
                $('#pnlPhoto').show()
                $('#imgPhoto').attr('src', result.file)
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $('#pnlLoading').hide()
                $('#pnlPhoto').show()
                $('#imgPhoto').hide()
                $('#pnlPhoto > .card-title').html('There was an error retrieving the photo.  Please try again.')
            }
        }
    )
}
